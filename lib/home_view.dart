import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'add_time_dialog.dart';
import 'models/time.dart';
import 'home.dart';
import 'list_time.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> implements HomeContract {
  Home home;
  List<Time> data = [];
  int timeDiff;
  int endOfDay;
  int remainingHours;
  int fulltime;
  int now;
  
  @override
  void initState() {
    super.initState();
    home = Home(this);
    timeDiff = 0;
    endOfDay = 0;
    remainingHours = 0;
    fulltime = DateTime.utc(2000, 1, 1, 9, 0, 0, 0, 0).millisecondsSinceEpoch
             - DateTime.utc(2000, 1, 1, 1, 0, 0, 0, 0).millisecondsSinceEpoch;
    
    home.processTimes().then( (td) {
      timeDiff = td;

      DateTime dtNow = DateTime.now();
      now = DateTime.utc(2000, 01, 01, dtNow.hour, dtNow.minute, 0, 0, 0).millisecondsSinceEpoch;

      home.getRemainingHoursOfDay(td, fulltime, now).then( (rhod) {
        remainingHours = rhod;
      });

      home.getEndOfDay(td, fulltime, now, data != null ? data.length > 2 : false).then( (eod) {
        endOfDay = eod;
      });
    });
  }

  @override
  void screenUpdate() {
    home.processTimes().then( (td) {
      timeDiff = td;

      DateTime dtNow = DateTime.now();
      now = DateTime.utc(2000, 01, 01, dtNow.hour, dtNow.minute, 0, 0, 0).millisecondsSinceEpoch;

      home.getRemainingHoursOfDay(td, fulltime, now).then( (rhod) {
        remainingHours = rhod;
        setState(() {});
      });

      home.getEndOfDay(td, fulltime, now, data != null ? data.length > 2 : false).then( (eod) {
        endOfDay = eod;
        setState(() {});
      });    
    });
  }

  displayRecord() {        
    home.processTimes().then( (td) {
      timeDiff = td;
      
      DateTime dtNow = DateTime.now();
      now = DateTime.utc(2000, 01, 01, dtNow.hour, dtNow.minute, 0, 0, 0).millisecondsSinceEpoch;

      home.getRemainingHoursOfDay(td, fulltime, now).then( (rhod) {
        remainingHours = rhod;
        setState(() {});
      });

      home.getEndOfDay(td, fulltime, now, data != null ? data.length > 2 : false).then( (eod) {
        endOfDay = eod;
        setState(() {});
      });    
    });
  }

  Widget _buildTitle(BuildContext context) {
    var horizontalTitleAlignment =
        Platform.isIOS ? CrossAxisAlignment.center : CrossAxisAlignment.center;

    return InkWell(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 12.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: horizontalTitleAlignment,
          children: <Widget>[
            Text(
              'Horários',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Colors.white,
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future _openAddTimeDialog() async {
    showDialog(
      context: context,
      builder: (BuildContext context) =>
          AddTimeDialog().buildAboutDialog(context, false, this, null),
    );

    setState(() {});
  }

  List<Widget> _buildActions() {
    return <Widget>[
      IconButton(
        icon: const Icon(
          Icons.add_alarm,
          color: Colors.white,
        ),
        onPressed: _openAddTimeDialog,
      ),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[700],
      appBar: AppBar(
        title: _buildTitle(context),
        actions: _buildActions(),
      ),
      body: FutureBuilder<List<Time>>(
        future: home.getTimes(),
        builder: (context, snapshot) {
          if (snapshot.hasError) print(snapshot.error);
          
          data = snapshot.data;
          
          if (data != null) {
            data.sort(
              (a, b) => a.getTime().compareTo( b.getTime() )
            );
          }

          int sizeOfData = data != null ? data.length : 0;
          
          return snapshot.hasData ? Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              SizedBox(height: 24),
              Center(
                child: Text(
                  _formatTime(timeDiff),
                  style: TextStyle(
                    fontSize: 32,
                    fontWeight: FontWeight.w700,
                    color: Colors.amber[200],
                  )
                )
              ),
              snapshot.hasData && sizeOfData % 2 != 0 ? Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Text(
                     'Previsão de saída: ',
                      style: TextStyle(
                        fontSize: 24,
                        fontWeight: FontWeight.w700,
                        color: Colors.amber,
                    )
                  ),
                  Text(
                    _getFinishTime(endOfDay),
                      style: TextStyle(
                        fontSize: 24,
                        fontWeight: FontWeight.w700,
                        color: Colors.amber[200],
                    )
                  )
                ]
              ) : Container(),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Text(
                     'Restam: ',
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w700,
                        color: Colors.red,
                    )
                  ),
                  Text(
                    _getFinishTime(remainingHours),
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w700,
                        color: Colors.red[200],
                    )
                  )
                ]
              ),
              SizedBox(height: 24),
              Container(
                height: 61 * 1.0 * data.length,
                child: Padding(
                  padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                  child: Card(
                    color: Colors.grey[500],
                    elevation: 4.0,
                    child: SizedBox(
                      height: 2,
                      child: TimeList(data, home),
                    )
                  )                
                )
              ),
            ]
          ) : Column(  
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Center(
                child: Text(
                  'Lista vazia!',
                  style: TextStyle(
                    color: Colors.redAccent,
                    fontSize: 32
                  )
                )
              ) 
            ]
          );            
        },
      ),
      floatingActionButton: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Transform.scale(
              scale: 1,
              child: FloatingActionButton(
                backgroundColor: Colors.red[400],
                onPressed: () => _deleteAll(),
                tooltip: 'Limpar',
                child: Icon(
                  Icons.delete_forever,          
                  color: Colors.white,
                ),
              ),
            ),
            SizedBox(height: 20),
            Transform.scale(
              scale: 1.2,
              child: FloatingActionButton(
                backgroundColor: Colors.amber,
                onPressed: () => displayRecord(),
                tooltip: 'Atualizar',
                child: Icon(
                  Icons.refresh,          
                  color: Colors.white,
                ),
              ),
            ),
          ],
        )
      )
    );
  }

  String _formatTime(int time) {
    return '${DateTime.fromMillisecondsSinceEpoch(time).toUtc().hour < 10 ? '0' + DateTime.fromMillisecondsSinceEpoch(time).toUtc().hour.toString() : DateTime.fromMillisecondsSinceEpoch(time).toUtc().hour }:' +
           '${DateTime.fromMillisecondsSinceEpoch(time).toUtc().minute < 10 ? '0' + DateTime.fromMillisecondsSinceEpoch(time).toUtc().minute.toString() : DateTime.fromMillisecondsSinceEpoch(time).toUtc().minute }';
  }

  void _deleteAll() {
    home.showConfirmDialog('Excluir todos os registros?', context, true, null);
  }  

  String _getFinishTime(int endOfDay) {
    return '${DateTime.fromMillisecondsSinceEpoch(endOfDay).toUtc().hour < 10 ? '0' + DateTime.fromMillisecondsSinceEpoch(endOfDay).toUtc().hour.toString() : DateTime.fromMillisecondsSinceEpoch(endOfDay).toUtc().hour }:' +
            '${DateTime.fromMillisecondsSinceEpoch(endOfDay).toUtc().minute < 10 ? '0' + DateTime.fromMillisecondsSinceEpoch(endOfDay).toUtc().minute.toString() : DateTime.fromMillisecondsSinceEpoch(endOfDay).toUtc().minute }';
  }
}