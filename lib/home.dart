import 'package:flutter/material.dart';

import 'database/database_helper.dart';
import 'models/time.dart';
import 'dart:async';

abstract class HomeContract {
  void screenUpdate();
}

class Home {
  HomeContract _view;
  
  DatabaseHelper db = DatabaseHelper();
  Home(this._view);

  void deleteAll() {
    DatabaseHelper db = DatabaseHelper();
    db.deleteAllTimes();
    updateScreen();
  }

  void delete(Time time) {
    DatabaseHelper db = DatabaseHelper();
    db.deleteTime(time.id);
    updateScreen();
  }

  Future<int> getEndOfDay(int timeDiff, int fulltime, int now, bool afterLunch) async {
    print('DEBUG (End) Ja se foram: ');
    print('DEBUG (End) ${DateTime.fromMillisecondsSinceEpoch(timeDiff).toUtc().hour < 10 ? '0' + DateTime.fromMillisecondsSinceEpoch(timeDiff).toUtc().hour.toString() : DateTime.fromMillisecondsSinceEpoch(timeDiff).toUtc().hour }:' +
      '${DateTime.fromMillisecondsSinceEpoch(timeDiff).toUtc().minute < 10 ? '0' + DateTime.fromMillisecondsSinceEpoch(timeDiff).toUtc().minute.toString() : DateTime.fromMillisecondsSinceEpoch(timeDiff).toUtc().minute }');

    if (!afterLunch) {
      fulltime = DateTime.utc(2000, 1, 1, 10, 0, 0, 0, 0).millisecondsSinceEpoch
             - DateTime.utc(2000, 1, 1, 1, 0, 0, 0, 0).millisecondsSinceEpoch;
    }

    int remainingHours = fulltime - timeDiff;
    print('DEBUG (End) Faltam: ');
    print('DEBUG (End) ${DateTime.fromMillisecondsSinceEpoch(remainingHours).toUtc().hour < 10 ? '0' + DateTime.fromMillisecondsSinceEpoch(remainingHours).toUtc().hour.toString() : DateTime.fromMillisecondsSinceEpoch(remainingHours).toUtc().hour }:' +
      '${DateTime.fromMillisecondsSinceEpoch(remainingHours).toUtc().minute < 10 ? '0' + DateTime.fromMillisecondsSinceEpoch(remainingHours).toUtc().minute.toString() : DateTime.fromMillisecondsSinceEpoch(remainingHours).toUtc().minute }');
    
    print('DEBUG (End) Agora: ');
    print('DEBUG (End) ${DateTime.fromMillisecondsSinceEpoch(now).toUtc().hour < 10 ? '0' + DateTime.fromMillisecondsSinceEpoch(now).toUtc().hour.toString() : DateTime.fromMillisecondsSinceEpoch(now).toUtc().hour }:' +
      '${DateTime.fromMillisecondsSinceEpoch(now).toUtc().minute < 10 ? '0' + DateTime.fromMillisecondsSinceEpoch(now).toUtc().minute.toString() : DateTime.fromMillisecondsSinceEpoch(now).toUtc().minute }');

    return now + remainingHours;
  }

    Future<int> getRemainingHoursOfDay(int timeDiff, int fulltime, int now) async {
    print('DEBUG (Rem) Ja se foram: ');
    print('DEBUG (Rem) ${DateTime.fromMillisecondsSinceEpoch(timeDiff).toUtc().hour < 10 ? '0' + DateTime.fromMillisecondsSinceEpoch(timeDiff).toUtc().hour.toString() : DateTime.fromMillisecondsSinceEpoch(timeDiff).toUtc().hour }:' +
      '${DateTime.fromMillisecondsSinceEpoch(timeDiff).toUtc().minute < 10 ? '0' + DateTime.fromMillisecondsSinceEpoch(timeDiff).toUtc().minute.toString() : DateTime.fromMillisecondsSinceEpoch(timeDiff).toUtc().minute }');

    int remainingHours = fulltime - timeDiff;
    print('DEBUG (Rem) Faltam: ');
    print('DEBUG (Rem) ${DateTime.fromMillisecondsSinceEpoch(remainingHours).toUtc().hour < 10 ? '0' + DateTime.fromMillisecondsSinceEpoch(remainingHours).toUtc().hour.toString() : DateTime.fromMillisecondsSinceEpoch(remainingHours).toUtc().hour }:' +
      '${DateTime.fromMillisecondsSinceEpoch(remainingHours).toUtc().minute < 10 ? '0' + DateTime.fromMillisecondsSinceEpoch(remainingHours).toUtc().minute.toString() : DateTime.fromMillisecondsSinceEpoch(remainingHours).toUtc().minute }');
    
    return remainingHours;
  }

  Future<int> processTimes() async {
    List<Time> times = await getTimes();

    if (times != null) {
      times.sort(
        (a, b) => a.getTime().compareTo( b.getTime() )
      );
    }
    
    int timeDiffAux = 0;

    if (times != null && times.length > 0) {
      times.forEach( (t) => 
        print(
          'DEBUG ${t.id}: ' +
          '${DateTime.fromMillisecondsSinceEpoch(t.getTime()).toUtc().hour < 10 ? '0' + DateTime.fromMillisecondsSinceEpoch(t.getTime()).toUtc().hour.toString() : DateTime.fromMillisecondsSinceEpoch(t.getTime()).toUtc().hour }:' +
          '${DateTime.fromMillisecondsSinceEpoch(t.getTime()).toUtc().minute < 10 ? '0' + DateTime.fromMillisecondsSinceEpoch(t.getTime()).toUtc().minute.toString() : DateTime.fromMillisecondsSinceEpoch(t.getTime()).toUtc().minute }'
        ) 
      );

      List<int> aux = List<int>();
      for(var i = 0; i < times.length; i++) {
        if(aux.length < 2) {
          aux.add(times[i].getTime());
        
          if (i + 1 == times.length && aux.length != 2) {
            DateTime now = DateTime.now();
            now = DateTime.utc(2000, 01, 01, now.hour, now.minute, 0, 0, 0);
            aux.add(now.millisecondsSinceEpoch);
          }
        }

        if (aux.length == 2) {
          timeDiffAux += aux[1] - aux[0];
          aux.clear();
        }
      }

      Duration total = DateTime.fromMillisecondsSinceEpoch(timeDiffAux)
                               .toUtc()
                               .difference(DateTime.utc(1970, 01, 01, 0, 0, 0, 0, 0));

      List<String> durationArr = total.toString().split(':');

      print('Hrs: ${(int.parse(durationArr[0]) > 10 ? durationArr[0] : '0' + durationArr[0])}' + 
            ':' +
            '${durationArr[1]}' );
      
    } 
      
    return timeDiffAux;
  }

  Future<List<Time>> getTimes() {
    return db.getTimes();
  }

  void updateScreen() {
    _view.screenUpdate();
  }

  showConfirmDialog(String text, BuildContext context, bool deleteAll, Time timeToDelete) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: Colors.grey[700],
          title: Center(
            child: Text(
              text,
              style: TextStyle(
                color: Colors.white,
                fontSize: 16
              ),
            )
          ),
          actions: <Widget>[
            FlatButton(
              child: Text(
                'CANCELAR',
                style: TextStyle(
                  color: Colors.white
                ),
              ),
              onPressed: () => Navigator.of(context).pop(),
            ),
            FlatButton(
              child: Text('CONFIRMAR',
                style: TextStyle(
                  color: Colors.amber[300]
                ),
              ),
              onPressed: () {
                Navigator.of(context).pop();
                if (deleteAll)
                  this.deleteAll();
                else
                  this.delete(timeToDelete);
              },
            )
          ],
        );
      }
    );
  }
}