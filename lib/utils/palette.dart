import 'package:flutter/material.dart';

class Palette {
  Palette._(); // this basically makes it so you can instantiate this class
  static const Color lightBlue = Color(0xFFDCF7FA);
  static const Color mediumBlue = Color(0xFF00ABBA);
  static const Color strongBlue = Color(0xFF007B8A);
  static const Color darkBlue = Color(0xFF005660);
  static const Color black = Color(0xFF1A1A1A);
  static const Color white = Color(0xFFFFFFFF);
  static const Color lightGrey = Color(0xFFD3D3D3);
  static const Color darkGrey = Color(0xFF1F1B24);
  static const Color transparent = Colors.transparent;
}
