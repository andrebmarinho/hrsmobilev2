import 'package:flutter/material.dart';
import 'home_view.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Controle de Ponto',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryColor: Colors.amber[600],// const Color(0xFF02BB9F),
        primaryColorDark: Colors.amber[800],//const Color(0xFF167F67),
        accentColor: Colors.amber[200]//const Color(0xFFFFAD32),
      ),
      home: MyHomePage(title: 'Controle de Ponto'),
    );
  }
}