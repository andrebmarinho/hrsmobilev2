final String tableTimes = 'times';
final String columnTimeId = '_id';
final String columnTime = 'time';

class Time {
  int id;
  int _time;

  Time(this._time);

  Time.fromMap(Map<String, dynamic> map) {
    id = map[columnTimeId];
    _time = map[columnTime];
  }

  Map<String, dynamic> toMap() {
    var map = <String, dynamic> {
      columnTime: _time,
    };

    if (id != null) {
      map[columnTimeId] = id;
    }

    return map;
  }

  int getTime() {
    return _time;
  }
}