import 'dart:async';
import 'package:flutter/material.dart';
import 'database/database_helper.dart';
import 'hr_dropdown.dart';
import 'models/time.dart';

class AddTimeDialog {
  Time time;

  int selectedHour;
  int selectedMin;
  int timeDiff;
  
  List<Time> allTimes;
  List<Time> selectedTimes;
  List<String> durationArr;

  Widget buildAboutDialog(BuildContext context, bool isEdit, dynamic _myHomePageState, Time time) {
    if (time != null) {
      this.time = time;
      selectedHour = DateTime.fromMillisecondsSinceEpoch(time.getTime()).toUtc().hour;
      selectedMin = DateTime.fromMillisecondsSinceEpoch(time.getTime()).toUtc().minute;
    }

    return AlertDialog(
      backgroundColor: Colors.grey[700],
      title: Center(
        child: Text(
          isEdit ? 'EDITAR HORÁRIO' : 'ADICIONAR HORÁRIO',
          style: TextStyle(
            color: Colors.white
          ),
        )
      ),
      content: SingleChildScrollView(
        child: Center(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  HrDropDown(
                    maxVal: 24,
                    selected: selectedHour,
                    changedDropDown: (value) {
                      selectedHour = value;
                    }, 
                  ),
                  HrDropDown(
                    maxVal: 60,
                    selected: selectedMin,
                    changedDropDown: (value) {
                      selectedMin = value;
                    }, 
                  ),
                ]
              ),
            ]
          )
        )
      ),
      actions: <Widget>[
        FlatButton(
          child: Text(
            'CANCELAR',
            style: TextStyle(
              color: Colors.white
            ),
          ),
          onPressed: () => Navigator.of(context).pop(),
        ),
        FlatButton(
          child: Text(
            isEdit ? 'EDITAR' : 'ADICIONAR',
            style: TextStyle(
              color: Colors.amber[400]
            ),
          ),
          onPressed: () {
            selectedHour = selectedHour ?? 0;
            selectedMin = selectedMin ?? 0;
            addRecord(isEdit);
            _myHomePageState.displayRecord();
            Navigator.of(context).pop();
          },
        )
      ],
    );
  }
  
  Widget getAppBorderButton(String buttonLabel, EdgeInsets margin) {
    return Container(
      margin: margin,
      padding: EdgeInsets.all(8.0),
      alignment: FractionalOffset.center,
      decoration: BoxDecoration(
        border: Border.all(
          color: Colors.white// const Color(0xFF28324E)
        ),
        borderRadius: BorderRadius.all(const Radius.circular(6.0)),
      ),
      child: Text(
        buttonLabel,
        style: TextStyle(
          color: Colors.white,
          fontSize: 20.0,
          fontWeight: FontWeight.w500,
          letterSpacing: 0.3,
        ),
      ),
    );
  }

  Future addRecord(bool isEdit) async {
    DatabaseHelper dbHelper = DatabaseHelper();

    Time time = Time(DateTime.utc(2000, 1, 1, selectedHour, selectedMin, 0, 0, 0).millisecondsSinceEpoch);

    if (isEdit) {
      time.id = this.time.id;
      await dbHelper.updateTime(time);
    } else {
      await dbHelper.saveTime(time);
    }

    selectedHour = null;
    selectedMin = null;
  }
}