import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:horarios2/add_time_dialog.dart';
import 'models/time.dart';
import 'home.dart';

class TimeList extends StatelessWidget {
  final List<Time> times;
  final Home home;

  TimeList(
    this.times,
    this.home, {
    Key key
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: times == null ? 0 : times.length,
      itemBuilder: (BuildContext context, int index) {
        return InkWell(
          onTap: () => edit(times[index], context),
          child: Card(
            color: Colors.grey[500],          
            child: Container(
              height: 50,
              child: Center(
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: Padding(
                        padding: EdgeInsets.all(10.0),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              times[index].id.toString(),
                              style: TextStyle(
                                fontSize: 20.0,
                                color: Colors.amber[200]
                              ),
                            ),
                            SizedBox(width: 32),
                            Text(
                              _formatTime(times[index].getTime()),
                              style: TextStyle(
                                fontSize: 20.0, 
                                fontWeight: FontWeight.w500,
                                color: Colors.white
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        IconButton(
                          icon: Icon(
                            Icons.delete,
                            color: Colors.amber[200]
                          ),
                          onPressed: () => _delete(times[index], context)
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              padding: const EdgeInsets.fromLTRB(10.0, 0.0, 0.0, 0.0)
            ),
          )
        );
      });
  }

  displayRecord() {
    home.updateScreen();
  }

  edit(Time time, BuildContext context) {
    showDialog(
      context: context,
      builder: (BuildContext context) =>
        AddTimeDialog().buildAboutDialog(context, true, this, time)
    );
    home.updateScreen();
  }

  String _formatTime(int time) {
    return '${DateTime.fromMillisecondsSinceEpoch(time).toUtc().hour < 10 ? '0' + DateTime.fromMillisecondsSinceEpoch(time).toUtc().hour.toString() : DateTime.fromMillisecondsSinceEpoch(time).toUtc().hour }:' +
           '${DateTime.fromMillisecondsSinceEpoch(time).toUtc().minute < 10 ? '0' + DateTime.fromMillisecondsSinceEpoch(time).toUtc().minute.toString() : DateTime.fromMillisecondsSinceEpoch(time).toUtc().minute }';
  }

  void _delete(Time time, BuildContext context) {
    home.showConfirmDialog('Excluir o registro ${time.id}?', context, false, time);
  }  
}