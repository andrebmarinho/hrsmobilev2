import 'package:flutter/material.dart';

class HrDropDown extends StatefulWidget {
  final int selected;
  final int maxVal;
  final Function changedDropDown;

  const HrDropDown({
    Key key,
    @required this.selected,
    @required this.maxVal,
    @required this.changedDropDown
  }) : super(key: key);

  @override
  _HrDropDownState createState() => _HrDropDownState();
}

class _HrDropDownState extends State<HrDropDown> { 
  int _value;

  @override
  void initState() {
    _value = widget.selected ?? 0;

    super.initState();
  }

  @override
  Widget build(context) {
    return Padding(
      padding: EdgeInsets.all(8),
      child: Theme(
        data: Theme.of(context).copyWith(
          canvasColor: Colors.grey[700]
        ),
        child: DropdownButton<int>(
          value: _value,                    
          items: Iterable<int>.generate(widget.maxVal).map((int value) {
            return DropdownMenuItem<int>(
              value: value,
              child: Text(
                value >= 10 ? 
                value.toString() :
                '0' + value.toString(),
                style: TextStyle(
                  fontSize: 32,
                  color: Colors.white
                ),
              ),
            );
          }).toList(),
          onChanged: (val) { 
            print('$val');
            setState(() => _value = val);
            this.widget.changedDropDown(val);
          },
          style: Theme.of(context).textTheme.title,
        )
      )
    );
  }
}