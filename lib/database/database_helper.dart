import 'dart:async';
import 'dart:io' as io;

import '../models/time.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';

class DatabaseHelper {
  static final DatabaseHelper _instance = DatabaseHelper.internal();
  factory DatabaseHelper() => _instance;
  static Database _db;

  Future<Database> get db async {
    if (_db != null) return _db;

    _db = await initDb();
    return _db;
  }

  DatabaseHelper.internal();

  initDb() async {
    io.Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, "database.db");
    return await openDatabase(path, version: 1, onCreate: _onCreate);
  }

  void _onCreate(Database db, int version) async {
    String sql =  ' CREATE TABLE $tableTimes(           ' +
                  ' $columnTimeId INTEGER PRIMARY KEY,  ' +
                  ' $columnTime INTEGER NOT NULL)       ';
    await db.execute(sql);
  }

  Future<int> saveTime(Time time) async {
    Database dbClient = await db;
    return await dbClient.insert(tableTimes, time.toMap());
  }

  Future<bool> updateTime(Time time) async {
    Database dbClient = await db;
    return await dbClient.update(
      tableTimes, 
      time.toMap(),
      where: '_id = ?',
      whereArgs: <int> [time.id]) > 0;
  }

  Future<List<Time>> getTimes() async {
    Database dbClient = await db;
    
    List<Map> maps = await dbClient.query(
      tableTimes,
      columns: [
        columnTimeId,
        columnTime
      ],    
    );
    
    if (maps.length > 0) {
      List<Time> times = List<Time>();
      maps.forEach( (m) => times.add(Time.fromMap(m)) );
    
      return times;
    }
    
    return null;
  }
  
  Future<int> deleteTime(int id) async {
    Database dbClient = await db;

    return await dbClient.rawDelete('DELETE FROM $tableTimes WHERE _id = ?', [id]);
  }

  Future<void> deleteAllTimes() async {
    Database dbClient = await db;

    await dbClient.delete(tableTimes);
  }

}